package it.org.qldjvm.springboot.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.qldjvm.springboot.Application;
import org.qldjvm.springboot.client.RootResourceClient;
import org.qldjvm.springboot.config.FeignConfig;
import org.qldjvm.springboot.model.HelloWorld;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@ActiveProfiles({"local", "test"})
public class RootResourceIT {

    @LocalServerPort
    protected int port;

    @Autowired
    private RootResourceClient rootResourceClient;

    @Test
    public void testGetHelloWorld() {

        FeignConfig feignConfig = new FeignConfig();
        rootResourceClient = feignConfig.getRootResourceClient(port);

        assertEquals(new HelloWorld("world"), rootResourceClient.getHello());
    }

    @Test
    public void testPostHelloWorld() {

        FeignConfig feignConfig = new FeignConfig();
        rootResourceClient = feignConfig.getRootResourceClient(port);

        assertEquals(new HelloWorld("world from post"), rootResourceClient.postGetHello());
    }
}

