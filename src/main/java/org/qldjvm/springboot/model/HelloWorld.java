package org.qldjvm.springboot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class HelloWorld {
    @JsonProperty
    private String hello;

    public HelloWorld() {
    }

    public HelloWorld(String hello) {
        this.hello = hello;
    }

    public String getHello() {
        return hello;
    }

    public HelloWorld setHello(String hello) {
        this.hello = hello;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HelloWorld that = (HelloWorld) o;
        return Objects.equals(hello, that.hello);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hello);
    }
}
