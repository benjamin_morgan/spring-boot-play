package org.qldjvm.springboot;


import org.qldjvm.springboot.config.FeignConfig;
import org.qldjvm.springboot.rest.RootResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(RootResource.class, args);
    }
}
