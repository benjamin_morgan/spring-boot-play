package org.qldjvm.springboot.client;

import feign.RequestLine;
import org.qldjvm.springboot.model.HelloWorld;

public interface RootResourceClient {

	@RequestLine(value = "GET hello/")
	HelloWorld getHello();
	
	@RequestLine(value = "POST hello/" )
	HelloWorld postGetHello();
}
