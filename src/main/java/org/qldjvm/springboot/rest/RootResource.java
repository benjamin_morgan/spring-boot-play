package org.qldjvm.springboot.rest;

import org.qldjvm.springboot.model.HelloWorld;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("hello")
@EnableAutoConfiguration
public class RootResource {

	@RequestMapping("/")
	HelloWorld home() {
		return new HelloWorld("world");
	}

	@RequestMapping(path = "/", method = RequestMethod.POST )
	HelloWorld postHome() {
		return new HelloWorld("world from post");
	}
}
