package org.qldjvm.springboot.config;

import feign.Feign;
import feign.okhttp.OkHttpClient;
import org.qldjvm.springboot.client.RootResourceClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

import java.util.concurrent.TimeUnit;

@Configuration
public class FeignConfig {

    @Bean
    public RootResourceClient getRootResourceClient(@Value("${server.port}") int serverPort) {
        RootResourceClient client = Feign.builder()
                .decoder(new JacksonDecoder())
                .encoder(new JacksonEncoder())
                .client(new OkHttpClient(
                        new okhttp3.OkHttpClient.Builder()
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .build()))
                .target(RootResourceClient.class, "http://localhost:" + serverPort);
        return client;
    }
}
